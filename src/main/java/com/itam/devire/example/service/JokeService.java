package com.itam.devire.example.service;

import com.itam.devire.example.exception.RecordNotFoundException;
import com.itam.devire.example.model.JokeEntity;
import com.itam.devire.example.repository.JokeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
public class JokeService {
    @Autowired
    JokeRepository repository;

    public List<JokeEntity> getAllJokes()
    {
        List<JokeEntity> result = (List<JokeEntity>) repository.findAll();

        if(result.size() > 0) {
            return result;
        } else {
            return new ArrayList<JokeEntity>();
        }
    }


    public JokeEntity getJokeById(Long id) throws RecordNotFoundException
    {
        Optional<JokeEntity> joke = repository.findById(id);

        if(joke.isPresent()) {
            return joke.get();
        } else {
            throw new RecordNotFoundException("No joke record exist for given id");
        }
    }

    public JokeEntity createOrUpdateEmployee(JokeEntity entity)
    {
        if(entity.getId()  == null)
        {
            entity = repository.save(entity);

            return entity;
        }
        else
        {
            Optional<JokeEntity> joke = repository.findById(entity.getId());

            if(joke.isPresent())
            {
                JokeEntity newEntity = joke.get();
                newEntity.setTitle(entity.getTitle());
                newEntity.setContent(entity.getContent());

                newEntity = repository.save(newEntity);

                return newEntity;
            } else {
                entity = repository.save(entity);

                return entity;
            }
        }
    }

    public void deleteJokeById(Long id) throws RecordNotFoundException
    {
        Optional<JokeEntity> joke = repository.findById(id);

        if(joke.isPresent())
        {
            repository.deleteById(id);
        } else {
            throw new RecordNotFoundException("No joke record exist for given id");
        }
    }



}
