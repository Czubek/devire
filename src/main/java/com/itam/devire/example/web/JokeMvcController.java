package com.itam.devire.example.web;

import com.itam.devire.example.exception.RecordNotFoundException;
import com.itam.devire.example.model.JokeEntity;
import com.itam.devire.example.service.JokeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/")
public class JokeMvcController {

    @Autowired
    JokeService service;

    @RequestMapping
    public String getAllJokes(Model model)
    {
        List<JokeEntity> list = service.getAllJokes();

        model.addAttribute("jokes", list);
        return "list-jokes";
    }

    @RequestMapping(path = {"/edit", "/edit/{id}"})
    public String editJokeById(Model model, @PathVariable("id") Optional<Long> id)
            throws RecordNotFoundException
    {
        if (id.isPresent()) {
            JokeEntity entity = service.getJokeById(id.get());
            model.addAttribute("joke", entity);
        } else {
            model.addAttribute("joke", new JokeEntity());
        }
        return "add-edit-joke";
    }

    @RequestMapping(path = "/delete/{id}")
    public String deleteJokeById(Model model, @PathVariable("id") Long id)
            throws RecordNotFoundException
    {
        service.deleteJokeById(id);
        return "redirect:/";
    }

    @RequestMapping(path = "/createJoke", method = RequestMethod.POST)
    public String createOrUpdateEmployee(JokeEntity joke)
    {
        service.createOrUpdateEmployee(joke);
        return "redirect:/";
    }
}
