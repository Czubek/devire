package com.itam.devire.example.repository;


import com.itam.devire.example.model.JokeEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JokeRepository extends CrudRepository<JokeEntity, Long> {
}
